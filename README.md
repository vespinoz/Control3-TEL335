# Getting Started 
 **Importante ejecutar el backend primero**
 
Luego de haber clonado este repositorio, ejecute el siguiente comando en la terminal, en la ubicación del proyecto, para instalar las dependencias. 
```
npm install
```
Luego, para iniciar la aplicación, ejecute:
```
npm start
```
Se va a abrir en su navegador localhost:3000 y va a poder usar la aplicación web.
