import { useState, useEffect } from "react"
import axios from "axios"
import SpinnerLoader from "../components/Spinner"
import PhrasesList from "../components/PhrasesList"
import Header from "../components/Header.js"
import Footer from "../components/Footer.js"

function InfoPanel () {

    const [loaded, setDataLoaded] = useState(false)
    const [phrasesData, setPhrasesData] = useState([])
    const [listaFavoritos] = useState([])   //Estado para guardar la lista cuando se renderice la pagina
    const url_api = 'http://localhost:3001/phrase'
    
    const nextPhrase = async () => {
        const result = await axios.get(url_api)
        setPhrasesData(result.data)
    }

    const favPhrase = (phrase) => {
        if (!listaFavoritos.includes(phrase.phrase)) {      // Verifica que el elemento no exista en el arreglo
            listaFavoritos.push(phrase.phrase)
            alert(`Frase agregada: ${phrase.phrase}`) 
        } else
            alert(`Frase ya esta agregada`)
    }

    const showFavorites = () =>{
        return listaFavoritos
    }

    useEffect(() => {
        const fetchData = async () => {
            if (!loaded) {
                const load_data = await axios.get(url_api)
                if (load_data.data) {
                    setDataLoaded(true)
                    setPhrasesData(load_data.data)
                }
            }
        }
        fetchData()
    })

    return (
        <div className="App">
            <SpinnerLoader dataLoaded={loaded} />
            <Header />
            <PhrasesList phrases={phrasesData} onNext={nextPhrase} onFav={favPhrase} onShow={showFavorites} />
            <Footer />
        </div>
    )
}

export default InfoPanel
