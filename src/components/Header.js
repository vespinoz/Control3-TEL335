import { Container, Row, Col} from "react-bootstrap"
import './Header.css';

function Header () {
    return (
        <section id='header'>
            <Container>
                <Row>
                    <Col>
                        <h1 class="display-4">Random Phraser</h1>
                        <p class="lead" align="left">To get a new phrase just click "Ver más".</p>
                    </Col>
                </Row>
            </Container>
        </section>
    )
}

export default Header