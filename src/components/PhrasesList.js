import { Container, Row, Col, Button, Modal } from "react-bootstrap"
import { useState} from "react"
import './bodyContent.css';

function PhrasesList (props) {
        
    // Estado para el cuadro de dialogo
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    if(props.phrases!== undefined)
    return (
        <section id='phrasesList'>
            <Container>
                <Row>
                    <Col align="left"><Button variant="primary" size="lg" onClick={() => props.onNext()}>Ver más</Button></Col>
                    <Col align="right"><Button variant="info" size="lg" onClick={handleShow}>Mis Frases Favoritas</Button></Col>
                </Row>
                <Row id="frase">
                    <Col>
                        <h2>{props.phrases.phrase}</h2>
                    </Col>
                </Row>
                <Row align="right">
                    <Col><Button variant="success" size="lg" onClick={() => props.onFav(props.phrases)}>Me gusta</Button></Col>
                </Row>
            </Container>
            <Modal.Dialog>
                <Modal show={show} onHide={handleClose} scrollable={true}>
                    <Modal.Header closeButton>
                        <Modal.Title>Mis frases favoritas</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ul className="arrow-styled">
                            {
                            props.onShow().map((phrase, index) => (
                                    <div key={index} className='row'>
                                        <Row>
                                            <Col><h4>{index+1} {phrase}</h4></Col>
                                        </Row>
                                    </div>
                                ))
                            }
                        </ul>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={handleClose}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </Modal.Dialog>
        </section>
    )
}

export default PhrasesList
